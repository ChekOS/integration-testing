const express = require('express');
const converter = require('./converter')
const app = express();
const port = 3000;

app.get('/', (req, res) => {
  res.send('OK!')
})

app.get('/hex-to-rgb', (req, res) => {
    const hex = req.query.hex;

    if (!hex.match(/^([0-9A-Fa-f]{3}){1,2}$/)) { //REGEX
      return res.status(400).json({ error: 'Invalid hex value' });
    }

    const rgb = converter.hexToRgb(hex);
    res.json(rgb);
});

if (process.env.NODE_ENV === 'test') {
  module.exports = app
} else {
  app.listen(port, () => {
    console.log(`Server:  http://localhost:${port}`)
  });
}

module.exports = app;