// test/converter.spec.js - Unit tests

const converter = require('../src/converter');
const { expect } = require('chai');


describe("Color Code Converter", () => {
    describe("Hex to RGB conversion", () => {
        it("Can convert colors", () => {
            const red = converter.hexToRgb('ff0000');
            const green = converter.hexToRgb('00ff00');
            const blue = converter.hexToRgb('0000ff');
            const purple = converter.hexToRgb('9332a8');
            const orange = converter.hexToRgb('ed8207');

            expect(red).to.deep.equal({ red: 255, green: 0, blue: 0 });
            expect(green).to.deep.equal({ red: 0, green: 255, blue: 0 });
            expect(blue).to.deep.equal({ red: 0, green: 0, blue: 255 });
            expect(purple).to.deep.equal({ red: 147, green: 50, blue: 168 });
            expect(orange).to.deep.equal({ red: 237, green: 130, blue: 7 });
        });
    });
});