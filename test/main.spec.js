const request = require('request');
const { expect } = require('chai');
const app = require('../src/main');
const port = 3000;

describe('Color Code Converter API', () => {
    let server;
    before("Start server before run tests", (done) => {
        server = app.listen(port, () => {
            console.log(`Server listening: http://localhost:${port}`);
            done();
        });
    });    

    describe('Hex to RGB conversion', () => {
        const validUrl = `http://localhost:${port}/hex-to-rgb?hex=9332a8`;
        const invalidUrl = `http://localhost:${port}/hex-to-rgb?hex=ff800x`; // Invalid hex format

        it('It returns the color in RGB for valid hex', (done) => {
            request(validUrl, (error, response, body) => {
                expect(body).to.equal('{"red":147,"green":50,"blue":168}');
                done();
            });
        });

        it('It returns an error message for invalid hex', (done) => {
            request(invalidUrl, (error, response, body) => {
                const parsedBody = JSON.parse(body);
                expect(parsedBody.error).to.equal('Invalid hex value');
                done();
            });
        });
    });

    after("Stop server after tests", (done) => {
        server.close();
        done();
    });
});
